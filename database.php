<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 11/11/2018
 * Time: 2:28 PM
 */

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "form_assignment_p1";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>