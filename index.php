<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 11/11/2018
 * Time: 1:39 PM
 */

require("database.php");

if(isset($_POST["submit"]))
{
    if(!empty($_POST["firstname"]) && strlen($_POST['firstname']) > 1 && ctype_alpha(str_replace(' ', '', $_POST["firstname"])))
    {
        $firstnameUT = strip_tags($_POST["firstname"]);
        $firstname = trim($firstnameUT);
    }
    else{
        $errorFN = "Make sure your firstname is filled in and only contains letters";
    }

    if(!empty($_POST["lastname"]) && strlen($_POST['lastname']) > 1 && ctype_alpha(str_replace(' ', '', $_POST["lastname"])))
    {
        $lastnameUT = strip_tags($_POST["lastname"]);
        $lastname = trim($lastnameUT);
    }
    else{
        $errorLN = "Make sure your lastname is filled in and only contains letters";
    }

    if(!empty($_POST["email"]) && substr_count($_POST["email"], '@') > 0 && substr_count($_POST["email"], '@') < 2)
    {
        $emailUT = strip_tags($_POST["email"]);
        $email = trim($emailUT);
    }
    else{
        $errorEmail = "Make sure you entered a correct mail address";
    }

    if(!empty($_POST["age"]) && is_numeric($_POST["age"]) && $_POST["age"] > -1 && $_POST["age"] < 100)
    {
        $ageUT = strip_tags($_POST["age"]);
        $age = trim($ageUT);
    }
    else{
        $errorAge = "Make sure you filled in a number between 0 and 100";
    }

    if(!empty($_POST["gender"]))
    {
        $genderUT = strip_tags($_POST["gender"]);
        $gender = trim($genderUT);
    }

    if(isset($firstname) && isset($lastname) && isset($email) && isset($age) && isset($gender))
    {
        $query =
            "
          INSERT INTO users (firstname, lastname, email, age, gender)
          VALUES (?,?,?,?,?)
        ";
        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $query);
        mysqli_stmt_bind_param($stmt, "sssis", $firstname, $lastname, $email, $age, $gender);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    header("Location: ./index.php");
    }
}
?>

<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    <div id="container">

        <div id="left">
            <div id="form">
                <form method="POST">
                    <label for="firstname">Firstname:</label>
                    <br>
                    <input type="text" id="firstname" name="firstname" placeholder="Fill in your firstname"
                           value="<?php
                           if(!empty($errorFN) || !empty($errorLN) || !empty($errorEmail) || !empty($errorAge))
                           {
                               echo(htmlspecialchars($_POST["firstname"]));
                           }
                           ?>"/>
                    <br>
                    <p class="errormsg">
                        <?php
                        if(isset($errorFN))
                        {
                            echo($errorFN);
                        }
                        ?>
                    </p>
                    <br>
                    <br>
                    <label for="lastname">Lastname:</label>
                    <br>
                    <input id="lastname" name="lastname" type="text" placeholder="Fill in your lastname"
                           value="<?php
                           if(!empty($errorFN) || !empty($errorLN) || !empty($errorEmail) || !empty($errorAge))
                           {
                               echo(htmlspecialchars($_POST["lastname"]));
                           }
                           ?>"/>
                    <br>
                    <p class="errormsg">
                        <?php
                            if(isset($errorLN))
                            {
                                echo($errorLN);
                            }
                        ?>
                    </p>
                    <br>
                    <br>
                    <label for="email">Email:</label>
                    <br>
                    <input id="email" name="email" type="email" placeholder="Fill in your email"
                           value="
                           <?php
                           if(!empty($errorFN) || !empty($errorLN) || !empty($errorEmail) || !empty($errorAge))
                           {
                               echo(htmlspecialchars($_POST["email"]));
                           }
                           ?>
                           "/>
                    <br>
                    <p class="errormsg">
                        <?php
                        if(isset($errorEmail))
                        {
                            echo($errorEmail);
                        }
                        ?>
                    </p>
                    <br>
                    <br>
                    <label for="age">Age:</label>
                    <br>
                    <input id="age" name="age" type="number" placeholder="Fill in your age"
                           value="<?php
                           if(!empty($errorFN) || !empty($errorLN) || !empty($errorEmail) || !empty($errorAge))
                               {
                                   echo(htmlspecialchars($_POST["age"]));
                               }
                           ?>"
                    />
                    <br>
                    <p class="errormsg">
                        <?php
                        if(isset($errorAge))
                        {
                            echo($errorAge);
                        }
                        ?>
                    </p>
                    <br>
                    <br>
                    <label for="gender">Gender:</label>
                    <br>
                    <select name="gender">
                        <option value="male" selected>Male</option>
                        <option value="female">Female</option>
                    </select>
                    <br>
                    <br>
                    <input id="submit" name="submit" type="submit" value="Insert into table"/>
                </form>
            </div>
        </div>

        <div id="right">
            <div id="table">
                <?php
                $sql = "SELECT firstname, lastname, email, age, gender FROM users";
                $result = mysqli_query($conn, $sql);

                if ($result->num_rows > 0) {
                echo "<table style='width:50%; text-align: center;'>
                        <tr><th>Firstname</th><th>Lastname</th><th>Email</th><th>Age</th><th>Gender</th></tr>";
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                    echo "<tr>
                            <td>".htmlspecialchars($row["firstname"])."</td>
                            <td>".htmlspecialchars($row["lastname"])."</td>
                            <td>".htmlspecialchars($row["email"])."</td>
                            <td>".htmlspecialchars($row["age"])."</td>
                            <td>".htmlspecialchars($row["gender"])."</td>
                        </tr>";
                    }
                    echo "</table>";
                } else {
                echo "There are no records in the database.";
                }
                ?>
            </div>
        </div>

    </div>
</body>
